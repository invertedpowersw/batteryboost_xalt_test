/*
 * xalt_can.h
 *
 *  Created on: May 21, 2021
 *      Author: InvertedPower
 */

#ifndef INC_XALT_CAN_H_
#define INC_XALT_CAN_H_

#define MUX_MSG_COUNT 6

#define XALT_TIMEOUT_100MSEC 250;
#define XALT_TIMEOUT_200MSEC 600;
#define XALT_TIMEOUT_500MSEC 1250;
#define XALT_TIMEOUT_1SEC 2500;
#define XALT_TIMEOUT_5SEC 12500;

void xalt_can_init();
void xalt_can_send();
void xalt_can_check_rx();
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan);

uint64_t get_from_array(uint8_t* array,uint8_t len,uint8_t pos);

typedef enum{
	XALT_REQ_OFF 		=0,
	XALT_REQ_IDLE		=1,
	XALT_REQ_CHARGE 	=2,
	XALT_REQ_DISCHARGE 	=3

} XALT_Out_ReqMode;

typedef enum{
	XALT_OPEN 	=0,
	XALT_CLOSE 	=1,
	XALT_SNA 	=2,
	XALT_ERROR 	=3
} XALT_Out_ContactorCommand;

typedef enum{
	XALT_BALANCING_NOT_ALLOWED 	= 0,
	XALT_BALANCING_ALLOWED 		= 1
} XALT_Out_Balancing_Enable;


typedef enum{
	XALT_HVIL_PASS 	=0,
	XALT_HVIL_FAIL 	=1,
	XALT_HVIL_SNA 	=2,
	XALT_HVIL_ERROR =3
} XALT_In_HVILState;

typedef enum{
	XALT_CHGINT_INACTIVE 	=0,
	XALT_CHGINT_ACTIVE 		=1,
	XALT_CHGINT_SNA 		=2,
	XALT_CHGINT_ERROR		=3
} XALT_In_ChargerInterlockState;

typedef enum{
	XALT_NOT_CHARGING	 	=0,
	XALT_IS_CHARGING 		=1,
	XALT_CHARGER_SNA		=2,
	XALT_CHARGER_ERROR		=3
} XALT_In_ChargerState;

typedef enum{
	XALT_BALANCING_INACTIVE	 	=0,
	XALT_BALACING_ACTIVE 		=1,
	XALT_BALANCING_SNA			=2,
	XALT_BALANCING_ERROR		=3
} XALT_In_BalancingState;

typedef enum{
	XALT_ISORES_SOURCE_NCM 			=0,
	XALT_ISORES_SOURCE_THIS_CYCLE 	=1,
	XALT_ISORES_SNA 				=2,
	XALT_ISORES_ERROR				=3
} XALT_In_IsoResSource;

typedef enum{
	XALT_MODE_INIT 				=0,
	XALT_MODE_IDLE 				=1,
	XALT_MODE_PARTIAL_RUN 		=2,
	XALT_MODE_RUN				=3,
	XALT_MODE_FAULT 			=4,
	XALT_MODE_SLEEP 			=5,

} XALT_In_SysPowerMode;

typedef enum{
	XALT_E_OPEN_NOT_ACTIVE 	=0,
	XALT_E_OPEN_ACTIVE 		=1,
	XALT_E_OPEN_SNA 		=2,
	XALT_E_OPEN_ERROR		=3
} XALT_In_EmergencyOpenState;

typedef enum{
	XALT_APP_Command 			= 0x14ff4049,
	XALT_BATT_State 			= 0x14ff4064,
	XALT_BATT_CNC_Temp_Module 	= 0x14ff4164,
	XALT_BATT_Current 			= 0x14ff4264,
	XALT_BATT_SYS_Volt_String 	= 0x14ff4364,
	XALT_BATT_SYS_Volt_Cell 	= 0x14ff4464,
	XALT_BATT_SYS_SOC			= 0x14ff4564,
	XALT_BATT_CNC_SOC 			= 0x14ff4664,
	XALT_BATT_SYS_TEMP_MODULE	= 0x14ff4764,
	XALT_BATT_LIMITS_DISCHARGE	= 0x14ff4864,
	XALT_BATT_LIMITS_REGEN		= 0x14ff4964,
	XALT_BATT_LIMITS_CHARGER	= 0x14ff5064,
	XALT_BATT_ISORES			= 0x14ff5164,
	XALT_BATT_CNC_VOLT_STRING	= 0x14ff5264,
	XALT_BATT_SYS_SOHC 			= 0x14ff5364,
	XALT_BATT_DIAGNOSTIC_STRING	= 0x14ff5464,
	XALT_BATT_SYS_SOHR 			= 0x14ff5564,
	XALT_BATT_CNC_VOLT_CELL		= 0x14ff5664,
	XALT_BATT_HVIL		 		= 0x14ff5864,
	XALT_BATT_VERSION	 		= 0x18ff5764,
	XALT_BATT_DIAGNOSTIC_SYSTEM	= 0x18ff5964,
	XALT_UNKNOWN_UNUSED1		= 0x18FECA64,


} XALT_Can_MSGID;

typedef enum{
	TIMEOUT_XALT_BATT_State 			= 0,
	TIMEOUT_XALT_BATT_CNC_Temp_Module 	= 1,
	TIMEOUT_XALT_BATT_Current 			= 2,
	TIMEOUT_XALT_BATT_SYS_Volt_String 	= 3,
	TIMEOUT_XALT_BATT_SYS_Volt_Cell 	= 4,
	TIMEOUT_XALT_BATT_SYS_SOC			= 5,
	TIMEOUT_XALT_BATT_CNC_SOC 			= 6,
	TIMEOUT_XALT_BATT_SYS_TEMP_MODULE	= 7,
	TIMEOUT_XALT_BATT_LIMITS_DISCHARGE	= 8,
	TIMEOUT_XALT_BATT_LIMITS_REGEN		= 9,
	TIMEOUT_XALT_BATT_LIMITS_CHARGER	= 10,
	TIMEOUT_XALT_BATT_ISORES			= 11,
	TIMEOUT_XALT_BATT_CNC_VOLT_STRING	= 12,
	TIMEOUT_XALT_BATT_SYS_SOHC 			= 13,
	TIMEOUT_XALT_BATT_DIAGNOSTIC_STRING	= 14,
	TIMEOUT_XALT_BATT_SYS_SOHR 			= 15,
	TIMEOUT_XALT_BATT_CNC_VOLT_CELL		= 16,
	TIMEOUT_XALT_BATT_HVIL		 		= 17,
	TIMEOUT_XALT_BATT_VERSION	 		= 18,
	TIMEOUT_XALT_BATT_DIAGNOSTIC_SYSTEM	= 19,
}XALT_Can_Timeout;


typedef struct {
	XALT_Out_ContactorCommand APP_Contactor_Command;
	XALT_Out_ReqMode APP_State_ReqMode;
	XALT_Out_Balancing_Enable APP_Balancing_Enable_F;
	uint8_t APP_Comm_RollingCount;
}t_XALT_OutData;

typedef struct{
	uint8_t BATT_Comm_StringsAddressed;
	uint8_t BATT_Contactor_StringsConnected;
	uint8_t BATT_Comm_RollingCount;

	uint8_t LAST_RollingCount;
	uint8_t SECOND_LAST_RollingCount;

	float 	BATT_CNC_Temp_Module_Max;
	uint8_t BATT_CNC_Temp_Module_Max_StrId;
	uint8_t BATT_CNC_Temp_Module_Max_VtbId;
	uint8_t BATT_CNC_Temp_Module_Max_SubId;
	uint8_t BATT_CNC_Temp_Module_Max_V;
	float	BATT_CNC_Temp_Module_Min;
	uint8_t BATT_CNC_Temp_Module_Min_StrId;
	uint8_t BATT_CNC_Temp_Module_Min_VtbId;
	uint8_t BATT_CNC_Temp_Module_Min_SubId;
	uint8_t BATT_CNC_Temp_Module_Min_V;
	float   BATT_CNC_Temp_Module_Avg;
	uint8_t BATT_CNC_Temp_Module_Avg_V;

	float 	BATT_SYS_Temp_Module_Max;
	uint8_t BATT_SYS_Temp_Module_Max_StrId;
	uint8_t BATT_SYS_Temp_Module_Max_VtbId;
	uint8_t BATT_SYS_Temp_Module_Max_SubId;
	uint8_t BATT_SYS_Temp_Module_Max_V;
	float	BATT_SYS_Temp_Module_Min;
	uint8_t BATT_SYS_Temp_Module_Min_StrId;
	uint8_t BATT_SYS_Temp_Module_Min_VtbId;
	uint8_t BATT_SYS_Temp_Module_Min_SubId;
	uint8_t BATT_SYS_Temp_Module_Min_V;
	float   BATT_SYS_Temp_Module_Avg;
	uint8_t BATT_SYS_Temp_Module_Avg_V;

	float 	BATT_Current_System;
	float 	BATT_Current_String_Max;
	uint8_t BATT_Current_String_Max_StrId;
	float 	BATT_Current_String_Min;
	uint8_t BATT_Current_String_Min_StrId;
	uint8_t BATT_Current_System_V;
	uint8_t BATT_Current_String_Max_V;
	uint8_t BATT_Current_String_Min_V;

	float 	BATT_SYS_Volt_String_Avg;
	float 	BATT_SYS_Volt_String_Min;
	float 	BATT_SYS_Volt_String_Max;
	uint8_t BATT_SYS_Volt_String_Avg_V;
	uint8_t BATT_SYS_Volt_String_Min_V;
	uint8_t BATT_SYS_Volt_String_Min_StrId;
	uint8_t BATT_SYS_Volt_String_Max_V;
	uint8_t BATT_SYS_Volt_String_Max_StrId;

	float 	BATT_CNC_Volt_String_Avg;
	float 	BATT_CNC_Volt_String_Min;
	float 	BATT_CNC_Volt_String_Max;
	uint8_t BATT_CNC_Volt_String_Avg_V;
	uint8_t BATT_CNC_Volt_String_Min_V;
	uint8_t BATT_CNC_Volt_String_Min_StrId;
	uint8_t BATT_CNC_Volt_String_Max_V;
	uint8_t BATT_CNC_Volt_String_Max_StrId;

	float   BATT_SYS_Volt_Cell_Avg;
	float   BATT_SYS_Volt_Cell_Min;
	float   BATT_SYS_Volt_Cell_Max;
	uint8_t BATT_SYS_Volt_Cell_Avg_V;
	uint8_t BATT_SYS_Volt_Cell_Max_V;
	uint8_t BATT_SYS_Volt_Cell_Min_V;
	uint8_t BATT_SYS_Volt_Cell_Max_VtbId;
	uint8_t BATT_SYS_Volt_Cell_Max_StrId;
	uint8_t BATT_SYS_Volt_Cell_Min_VtbId;
	uint8_t BATT_SYS_Volt_Cell_Min_StrId;
	uint8_t BATT_SYS_Volt_Cell_Max_CellId;
	uint8_t BATT_SYS_Volt_Cell_Min_CellId;

	float   BATT_CNC_Volt_Cell_Avg;
	float   BATT_CNC_Volt_Cell_Min;
	float   BATT_CNC_Volt_Cell_Max;
	uint8_t BATT_CNC_Volt_Cell_Avg_V;
	uint8_t BATT_CNC_Volt_Cell_Max_V;
	uint8_t BATT_CNC_Volt_Cell_Min_V;
	uint8_t BATT_CNC_Volt_Cell_Max_VtbId;
	uint8_t BATT_CNC_Volt_Cell_Max_StrId;
	uint8_t BATT_CNC_Volt_Cell_Min_VtbId;
	uint8_t BATT_CNC_Volt_Cell_Min_StrId;
	uint8_t BATT_CNC_Volt_Cell_Max_CellId;
	uint8_t BATT_CNC_Volt_Cell_Min_CellId;

	float	BATT_SYS_SOC_String_Avg;
	float	BATT_SYS_SOC_String_Min;
	float	BATT_SYS_SOC_String_Max;
	uint8_t BATT_SYS_SOC_String_Avg_V;
	uint8_t BATT_SYS_SOC_String_Min_V;
	uint8_t BATT_SYS_SOC_String_Max_V;
	uint8_t BATT_SYS_SOC_String_Min_StrId;
	uint8_t BATT_SYS_SOC_String_Max_StrId;
	float	BATT_SYS_Energy_System;
	uint8_t	BATT_SYS_Energy_System_V;

	float	BATT_CNC_SOC_String_Avg;
	float	BATT_CNC_SOC_String_Min;
	float	BATT_CNC_SOC_String_Max;
	uint8_t BATT_CNC_SOC_String_Avg_V;
	uint8_t BATT_CNC_SOC_String_Min_V;
	uint8_t BATT_CNC_SOC_String_Max_V;
	uint8_t BATT_CNC_SOC_String_Min_StrId;
	uint8_t BATT_CNC_SOC_String_Max_StrId;
	float	BATT_CNC_Energy_System;
	uint8_t	BATT_CNC_Energy_System_V;

	float 	BATT_Limit_DischargeCrrnt;
	float	BATT_Limit_DischargePwr_LongTerm;
	float	BATT_Limit_DischargePwr_ShortTerm;
	float 	BATT_Limit_RegenPwr_MidTerm;

	float 	BATT_Limit_RegenCrrnt;
	float 	BATT_Limit_RegenPwr_LongTerm;
	float	BATT_Limit_RegenPwr_ShortTerm;
	float	BATT_Limit_DischargePwr_MidTerm;

	float	BATT_Limit_ChrgrVolt;
	float	BATT_Limit_ChrgrCrrnt;

	float	BATT_IsoRes_Min;
	float	BATT_IsoRes_Max;
	float 	BATT_IsoRes_Last;
	uint8_t BATT_IsoRes_Min_StrId;
	uint8_t BATT_IsoRes_Max_StrId;


	float 	BATT_SYS_SOHC_Module_Avg;
	float 	BATT_SYS_SOHC_Module_Min;
	float  	BATT_SYS_SOHC_Module_Max;
	uint8_t BATT_SYS_SOHC_Module_Min_StrId;
	uint8_t BATT_SYS_SOHC_Module_Min_VtbId;
	uint8_t BATT_SYS_SOHC_Module_Min_V;
	uint8_t BATT_SYS_SOHC_Module_Max_StrId;
	uint8_t BATT_SYS_SOHC_Module_Max_VtbId;
	uint8_t BATT_SYS_SOHC_Module_Max_V;
	uint8_t BATT_SYS_SOHC_Module_Avg_V;

	uint8_t 	MUX_String_ID;
	uint64_t 	SXX_Diag[MUX_MSG_COUNT];



	float 	BATT_SYS_SOHR_Module_Avg;
	float 	BATT_SYS_SOHR_Module_Min;
	float  	BATT_SYS_SOHR_Module_Max;
	uint8_t BATT_SYS_SOHR_Module_Min_StrId;
	uint8_t BATT_SYS_SOHR_Module_Min_VtbId;
	uint8_t BATT_SYS_SOHR_Module_Min_V;
	uint8_t BATT_SYS_SOHR_Module_Max_StrId;
	uint8_t BATT_SYS_SOHR_Module_Max_VtbId;
	uint8_t BATT_SYS_SOHR_Module_Max_V;
	uint8_t BATT_SYS_SOHR_Module_Avg_V;

	uint8_t BATT_HVIL_SinkVolt_V;
	uint8_t BATT_HVIL_SourceVolt_V;


	float	BATT_HVIL_SinkVolt;
	float	BATT_HVIL_SourceVolt;
	float	BATT_HVIL_SinkCrrnt;

	uint8_t MUX_Version_ID;

	uint64_t MCU_Version_String;
	uint64_t SXX_Version_String[MUX_MSG_COUNT]; // Contains individual sub versions (hw/sw) probably irrelevant for runtime

	uint8_t 	BATT_LastAddressed_StringID;
	uint64_t	BATT_Diagnostic_System_String; // Contains bits for each error

	XALT_In_HVILState hvilState;
	XALT_In_ChargerInterlockState chgIntState;
	XALT_In_ChargerState chargerState;
	XALT_In_BalancingState balanceSate;
	XALT_In_IsoResSource isoResSource;
	XALT_In_SysPowerMode xaltMode;
	XALT_In_EmergencyOpenState eOpenState;
}t_XALT_InData;

#endif /* INC_XALT_CAN_H_ */




