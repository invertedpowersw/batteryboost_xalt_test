/*
 * errors.h
 *
 *  Created on: May 24, 2021
 *      Author: InvertedPower
 */

#ifndef INC_ERRORS_H_
#define INC_ERRORS_H_

#define MAX_ERROR_COUNT 100



typedef enum{
	ERROR_NONE,
	ERROR_LOGONLY,
	ERROR_LOW,
	ERROR_HIGH,
	ERROR_CRITICAL
}t_errorSeverity;

typedef enum{
	ERROR_UNINIT,
	ERROR_GENERIC,
	ERROR_CAN_INIT,
	ERROR_XALT_CAN_TIMEOUT,
	ERROR_XALT_CAN_ROLLING_COUNT,
	ERROR_XALT_CAN_MUX_OUT_OF_BOUNDS,
	ERROR_XALT_UNKNOWN_MSG,
	ERROR_XALT_CANT_SEND,
}t_errorType;

typedef struct{
	t_errorSeverity sev;
	t_errorType type;
	uint32_t data1;
	uint32_t data2;
	uint32_t data3;
} t_error;


void reset_all_errors();
void set_error_struct(t_error e);
void set_error(t_errorType type,t_errorSeverity sev,uint32_t data1,uint32_t data2,uint32_t data3);

#endif /* INC_ERRORS_H_ */
