/*
 * xalt_can.c
 *
 *  Created on: May 21, 2021
 *      Author: InvertedPower
 */
#include "main.h"
#include "xalt_can.h"
#include "errors.h"

extern CAN_HandleTypeDef hcan1;
extern UART_HandleTypeDef huart2;

t_XALT_InData  XALT_InData;
t_XALT_OutData XALT_OutData;
uint16_t XALT_Timeout_Array[TIMEOUT_XALT_BATT_DIAGNOSTIC_SYSTEM+1];
uint8_t mux_val;

void xalt_can_init(){
	CAN_FilterTypeDef canFilter;
	canFilter.FilterIdHigh = 0x0;
	canFilter.FilterIdLow =  0x0;
	canFilter.FilterMaskIdHigh = 0;
	canFilter.FilterMaskIdLow = 0;
	canFilter.FilterFIFOAssignment = CAN_FILTER_FIFO0;
	canFilter.FilterActivation = CAN_FILTER_ENABLE;
	canFilter.FilterBank = 0;
	canFilter.FilterMode = CAN_FILTERMODE_IDMASK;
	canFilter.FilterScale = CAN_FILTERSCALE_32BIT;

	HAL_CAN_ConfigFilter(&hcan1, &canFilter);
	HAL_CAN_ActivateNotification(&hcan1,CAN_IT_RX_FIFO0_MSG_PENDING);
	HAL_CAN_Start(&hcan1);

	for(int i = 0;i<=TIMEOUT_XALT_BATT_DIAGNOSTIC_SYSTEM;i++){
		XALT_Timeout_Array[i] = XALT_TIMEOUT_5SEC;
	}
}

void xalt_can_send(){
	uint32_t txBufferSpace; // free buffer space
	uint32_t txMailboxUsed; // location of mailbox, unused but must supply pointer
	HAL_StatusTypeDef returnVal;


	uint8_t canData[8];
	uint64_t msgbuf;
	CAN_TxHeaderTypeDef canHeader;
	canHeader.IDE = 4;
	canHeader.DLC = 8;
	canHeader.RTR = 0;
	canHeader.ExtId = XALT_APP_Command;

	msgbuf = 0UL;
	msgbuf |= (uint64_t)(XALT_OutData.APP_Contactor_Command 	& 0x3)	<< 12;
	msgbuf |= (uint64_t)(XALT_OutData.APP_Balancing_Enable_F 	& 0x3) 	<< 22;
	// IsoRes_Enable not supported
	msgbuf |= (uint64_t)(++XALT_OutData.APP_Comm_RollingCount 	& 0x7)	<< 56; // note incremented here
	msgbuf |= (uint64_t)(XALT_OutData.APP_State_ReqMode 		& 0x1f) << 59;

	*((uint64_t*)canData) = msgbuf;

	txBufferSpace = HAL_CAN_GetTxMailboxesFreeLevel(&hcan1);
	if(txBufferSpace>0){
		returnVal = HAL_CAN_AddTxMessage(&hcan1, &canHeader,canData, &txMailboxUsed);
		if(returnVal != HAL_OK){
			set_error(ERROR_XALT_CANT_SEND,ERROR_LOGONLY,txBufferSpace,returnVal,0);
		}
	}else{
		set_error(ERROR_XALT_CANT_SEND,ERROR_LOGONLY,txBufferSpace,0,0);
	}



}
void xalt_can_check_rx()
{
	int i;
	for(i=0;i<=TIMEOUT_XALT_BATT_DIAGNOSTIC_SYSTEM;i++){
		if(XALT_Timeout_Array[i]>0){
			XALT_Timeout_Array[i]--;
		}else{
			set_error(ERROR_XALT_CAN_TIMEOUT, ERROR_HIGH, i, 0, 0);
		}
	}

}



void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan){
	//unsigned char serial_buf[100];
	//sprintf(serial_buf,"\n\r CAN Message: ID=%04X Length %d, Data= %02x %02x %02x %02x %02x %02x %02x %02x",canHeader.ExtId,canHeader.DLC,canData[0],canData[1],canData[2],canData[3],canData[4],canData[5],canData[6],canData[7]);
	//HAL_UART_Transmit(&huart2,serial_buf,strlen(serial_buf), 10000);

	uint8_t canData[8];

	CAN_RxHeaderTypeDef canHeader;
	HAL_CAN_GetRxMessage(&hcan1, 0, &canHeader,canData);



	switch(canHeader.ExtId) {
		case XALT_BATT_State:
			// internally keep track of rolling count
			XALT_InData.SECOND_LAST_RollingCount = XALT_InData.LAST_RollingCount;
			XALT_InData.LAST_RollingCount = XALT_InData.BATT_Comm_RollingCount;

			XALT_Timeout_Array[TIMEOUT_XALT_BATT_State] = XALT_TIMEOUT_100MSEC;
			XALT_InData.BATT_Contactor_StringsConnected 	= get_from_array(canData,5,8);
			XALT_InData.eOpenState 							= get_from_array(canData,2,14);
			XALT_InData.BATT_Comm_StringsAddressed 			= get_from_array(canData,5,16);
			XALT_InData.balanceSate 						= get_from_array(canData,2,22);
			XALT_InData.isoResSource						= get_from_array(canData,3,25);
			XALT_InData.hvilState 							= get_from_array(canData,2,30);
			XALT_InData.BATT_Comm_RollingCount 				= get_from_array(canData,3,56);
			XALT_InData.xaltMode 							= get_from_array(canData,5,59);
			break;
		case XALT_BATT_CNC_Temp_Module:
			XALT_Timeout_Array[TIMEOUT_XALT_BATT_CNC_Temp_Module] = XALT_TIMEOUT_1SEC;

			XALT_InData.BATT_CNC_Temp_Module_Max = ((float)  (get_from_array(canData, 8, 0)))-40.0f;
			XALT_InData.BATT_CNC_Temp_Module_Max_StrId 		= get_from_array(canData, 5, 8)+101;
			XALT_InData.BATT_CNC_Temp_Module_Max_VtbId 		= get_from_array(canData, 5, 13);
			XALT_InData.BATT_CNC_Temp_Module_Max_SubId 		= get_from_array(canData, 2, 18);
			XALT_InData.BATT_CNC_Temp_Module_Max_V 			= get_from_array(canData, 2, 20);

			XALT_InData.BATT_CNC_Temp_Module_Min = ((float)  (get_from_array(canData, 8, 24)))-40.0f;
			XALT_InData.BATT_CNC_Temp_Module_Min_StrId 		= get_from_array(canData, 5, 32)+101;
			XALT_InData.BATT_CNC_Temp_Module_Min_VtbId 		= get_from_array(canData, 5, 37);
			XALT_InData.BATT_CNC_Temp_Module_Min_SubId 		= get_from_array(canData, 2, 42);
			XALT_InData.BATT_CNC_Temp_Module_Min_V 			= get_from_array(canData, 2, 44);

			XALT_InData.BATT_CNC_Temp_Module_Avg = ((float)  (get_from_array(canData, 8, 48)))-40.0f;
			XALT_InData.BATT_CNC_Temp_Module_Avg_V 			= get_from_array(canData, 2, 56);

			break;
		case XALT_BATT_Current:
			XALT_Timeout_Array[TIMEOUT_XALT_BATT_Current] = XALT_TIMEOUT_100MSEC;
			XALT_InData.BATT_Current_System 		= (((float)get_from_array(canData,16,0))*0.5f) -16000.0f;
			XALT_InData.BATT_Current_String_Max		=  ((float)get_from_array(canData,10,16)-500.0f);
			XALT_InData.BATT_Current_String_Max_StrId 		 = get_from_array(canData, 5,26)+101;

			XALT_InData.BATT_Current_String_Min		=  ((float)get_from_array(canData,10,32)-500.0f);
			XALT_InData.BATT_Current_String_Min_StrId 		 = get_from_array(canData, 5,42)+101;

			XALT_InData.BATT_Current_String_Max_V 	= get_from_array(canData,2,50);
			XALT_InData.BATT_Current_String_Min_V 	= get_from_array(canData,2,52);

			break;
		case XALT_BATT_SYS_Volt_String:
			XALT_Timeout_Array[TIMEOUT_XALT_BATT_SYS_Volt_String] = XALT_TIMEOUT_100MSEC;

			XALT_InData.BATT_SYS_Volt_String_Max 		= (float)get_from_array(canData,11,0);
			XALT_InData.BATT_SYS_Volt_String_Max_StrId  = get_from_array(canData,5,11)+101;
			XALT_InData.BATT_SYS_Volt_String_Min 		= (float)get_from_array(canData,11,16);
			XALT_InData.BATT_SYS_Volt_String_Min_StrId  = get_from_array(canData,5,27)+101;

			XALT_InData.BATT_Current_String_Max_V 	= get_from_array(canData,2,32);
			XALT_InData.BATT_Current_String_Min_V 	= get_from_array(canData,2,34);

			XALT_InData.BATT_SYS_Volt_String_Avg 		= (float)get_from_array(canData,11,40);
			XALT_InData.BATT_SYS_Volt_String_Avg_V 		= (float)get_from_array(canData,2,51);

			break;
		case XALT_BATT_SYS_Volt_Cell:
			XALT_Timeout_Array[TIMEOUT_XALT_BATT_SYS_Volt_Cell] = XALT_TIMEOUT_100MSEC;

			XALT_InData.BATT_SYS_Volt_Cell_Max  = ((float)get_from_array(canData,10,0))*0.005f;
			XALT_InData.BATT_SYS_Volt_Cell_Max_StrId  = get_from_array(canData,5,10)+101;
			XALT_InData.BATT_SYS_Volt_Cell_Max_VtbId  = get_from_array(canData,5,15);
			XALT_InData.BATT_SYS_Volt_Cell_Max_CellId = get_from_array(canData,4,20);

			XALT_InData.BATT_SYS_Volt_Cell_Max  = ((float)get_from_array(canData,10,24))*0.005f;
			XALT_InData.BATT_SYS_Volt_Cell_Max_StrId  = get_from_array(canData,5,34)+101;
			XALT_InData.BATT_SYS_Volt_Cell_Max_VtbId  = get_from_array(canData,5,39);
			XALT_InData.BATT_SYS_Volt_Cell_Max_CellId = get_from_array(canData,4,44);

			XALT_InData.BATT_SYS_Volt_Cell_Avg = ((float)get_from_array(canData,10,48))*0.005f;

			XALT_InData.BATT_SYS_Volt_Cell_Max_V 	= get_from_array(canData,2,58);
			XALT_InData.BATT_SYS_Volt_Cell_Min_V 	= get_from_array(canData,2,60);
			XALT_InData.BATT_SYS_Volt_Cell_Avg_V 	= get_from_array(canData,2,62);

			break;
		case XALT_BATT_SYS_SOC:
			XALT_Timeout_Array[TIMEOUT_XALT_BATT_SYS_SOC] = XALT_TIMEOUT_500MSEC;

			XALT_InData.BATT_SYS_SOC_String_Avg = ((float)get_from_array(canData,8,0))*0.4f;

			XALT_InData.BATT_SYS_SOC_String_Min = ((float)get_from_array(canData,8,8))*0.4f;
			XALT_InData.BATT_SYS_SOC_String_Min_StrId 	= get_from_array(canData,5,16)+101;

			XALT_InData.BATT_SYS_SOC_String_Avg_V 		= get_from_array(canData,2,21);

			XALT_InData.BATT_SYS_SOC_String_Max = ((float)get_from_array(canData,8,24))*0.4f;
			XALT_InData.BATT_SYS_SOC_String_Max_StrId 	= get_from_array(canData,5,32)+101;

			XALT_InData.BATT_SYS_Energy_System_V = get_from_array(canData,2,37);
			XALT_InData.BATT_SYS_Energy_System = ((float)get_from_array(canData,16,40))*0.01f;

			XALT_InData.BATT_SYS_SOC_String_Max_V 		= get_from_array(canData,2,56);
			XALT_InData.BATT_SYS_SOC_String_Min_V 		= get_from_array(canData,2,58);

			break;
		case XALT_BATT_CNC_SOC:
			XALT_Timeout_Array[TIMEOUT_XALT_BATT_CNC_SOC] = XALT_TIMEOUT_500MSEC;

			XALT_InData.BATT_CNC_SOC_String_Avg = ((float)get_from_array(canData,8,0))*0.4f;

			XALT_InData.BATT_CNC_SOC_String_Min = ((float)get_from_array(canData,8,8))*0.4f;
			XALT_InData.BATT_CNC_SOC_String_Min_StrId 	= get_from_array(canData,5,16)+101;

			XALT_InData.BATT_CNC_SOC_String_Avg_V 		= get_from_array(canData,2,21);

			XALT_InData.BATT_CNC_SOC_String_Max = ((float)get_from_array(canData,8,24))*0.4f;
			XALT_InData.BATT_CNC_SOC_String_Max_StrId 	= get_from_array(canData,5,32)+101;

			XALT_InData.BATT_CNC_Energy_System_V = get_from_array(canData,2,37);
			XALT_InData.BATT_CNC_Energy_System = ((float)get_from_array(canData,16,40))*0.01f;

			XALT_InData.BATT_CNC_SOC_String_Max_V 		= get_from_array(canData,2,56);
			XALT_InData.BATT_CNC_SOC_String_Min_V 		= get_from_array(canData,2,58);

			break;
		case XALT_BATT_SYS_TEMP_MODULE:
			XALT_Timeout_Array[TIMEOUT_XALT_BATT_SYS_TEMP_MODULE] = XALT_TIMEOUT_1SEC;
			XALT_InData.BATT_SYS_Temp_Module_Max = ((float)  (get_from_array(canData, 8, 0)))-40.0f;
			XALT_InData.BATT_SYS_Temp_Module_Max_StrId 		= get_from_array(canData, 5, 8)+101;
			XALT_InData.BATT_SYS_Temp_Module_Max_VtbId 		= get_from_array(canData, 5, 13);
			XALT_InData.BATT_SYS_Temp_Module_Max_SubId 		= get_from_array(canData, 2, 18);
			XALT_InData.BATT_SYS_Temp_Module_Max_V 			= get_from_array(canData, 2, 20);

			XALT_InData.BATT_SYS_Temp_Module_Min = ((float)  (get_from_array(canData, 8, 24)))-40.0f;
			XALT_InData.BATT_SYS_Temp_Module_Min_StrId 		= get_from_array(canData, 5, 32)+101;
			XALT_InData.BATT_SYS_Temp_Module_Min_VtbId 		= get_from_array(canData, 5, 37);
			XALT_InData.BATT_SYS_Temp_Module_Min_SubId 		= get_from_array(canData, 2, 42);
			XALT_InData.BATT_SYS_Temp_Module_Min_V 			= get_from_array(canData, 2, 44);

			XALT_InData.BATT_SYS_Temp_Module_Avg = ((float)  (get_from_array(canData, 8, 48)))-40.0f;
			XALT_InData.BATT_SYS_Temp_Module_Avg_V 			= get_from_array(canData, 2, 56);

			break;
		case XALT_BATT_LIMITS_DISCHARGE:
			XALT_Timeout_Array[TIMEOUT_XALT_BATT_LIMITS_DISCHARGE] =  XALT_TIMEOUT_1SEC;

			XALT_InData.BATT_Limit_DischargeCrrnt = ((float)get_from_array(canData,16,0))*0.5f;
			XALT_InData.BATT_Limit_DischargePwr_ShortTerm 	= get_from_array(canData,12,16);
			XALT_InData.BATT_Limit_DischargePwr_MidTerm 	= get_from_array(canData,12,32);
			XALT_InData.BATT_Limit_DischargePwr_LongTerm 	= get_from_array(canData,12,48);

			break;
		case XALT_BATT_LIMITS_REGEN:
			XALT_Timeout_Array[TIMEOUT_XALT_BATT_LIMITS_REGEN] = XALT_TIMEOUT_1SEC;

			XALT_InData.BATT_Limit_RegenCrrnt = ((float)get_from_array(canData,16,0))*0.5f;
			XALT_InData.BATT_Limit_RegenPwr_ShortTerm 	= get_from_array(canData,12,16);
			XALT_InData.BATT_Limit_RegenPwr_MidTerm 	= get_from_array(canData,12,32);
			XALT_InData.BATT_Limit_RegenPwr_LongTerm 	= get_from_array(canData,12,48);

			break;
		case XALT_BATT_LIMITS_CHARGER:
			XALT_Timeout_Array[TIMEOUT_XALT_BATT_LIMITS_CHARGER] = XALT_TIMEOUT_1SEC;

			XALT_InData.BATT_Limit_ChrgrCrrnt 	= ((float)get_from_array(canData,16,0))*0.5f;
			XALT_InData.BATT_Limit_ChrgrVolt 	= (float)get_from_array(canData,11,16);
			XALT_InData.chgIntState 	= get_from_array(canData,2,32);
			XALT_InData.chargerState 	= get_from_array(canData,2,34);

			break;
		case XALT_BATT_ISORES:
			XALT_Timeout_Array[TIMEOUT_XALT_BATT_ISORES] = XALT_TIMEOUT_100MSEC;

			XALT_InData.BATT_IsoRes_Last = ((float)get_from_array(canData,16,0))*10.0f;

			XALT_InData.BATT_IsoRes_Max	 = ((float)get_from_array(canData,16,16))*10.0f;
			XALT_InData.BATT_IsoRes_Max_StrId = get_from_array(canData,5,32)+ 101;

			XALT_InData.BATT_IsoRes_Min	 = ((float)get_from_array(canData,16,40))*10.0f;
			XALT_InData.BATT_IsoRes_Min_StrId = get_from_array(canData,5,56)+ 101;


			break;
		case XALT_BATT_CNC_VOLT_STRING:
			XALT_Timeout_Array[TIMEOUT_XALT_BATT_CNC_VOLT_STRING] = XALT_TIMEOUT_100MSEC;

			XALT_InData.BATT_CNC_Volt_String_Max 		= (float)get_from_array(canData,11,0);
			XALT_InData.BATT_CNC_Volt_String_Max_StrId  = get_from_array(canData,5,11)+101;
			XALT_InData.BATT_CNC_Volt_String_Min 		= (float)get_from_array(canData,11,16);
			XALT_InData.BATT_CNC_Volt_String_Min_StrId  = get_from_array(canData,5,27)+101;

			XALT_InData.BATT_Current_String_Max_V 	= get_from_array(canData,2,32);
			XALT_InData.BATT_Current_String_Min_V 	= get_from_array(canData,2,34);

			XALT_InData.BATT_CNC_Volt_String_Avg 		= (float)get_from_array(canData,11,40);
			XALT_InData.BATT_CNC_Volt_String_Avg_V 		= (float)get_from_array(canData,2,51);

			break;
		case XALT_BATT_SYS_SOHC:
			XALT_Timeout_Array[TIMEOUT_XALT_BATT_SYS_SOHC] = XALT_TIMEOUT_5SEC;

			XALT_InData.BATT_SYS_SOHC_Module_Max = ((float)get_from_array(canData,10,0))*0.1f;
			XALT_InData.BATT_SYS_SOHC_Module_Max_StrId  = get_from_array(canData,5,10)+101;
			XALT_InData.BATT_SYS_SOHC_Module_Max_VtbId 	= get_from_array(canData,5,16);
			XALT_InData.BATT_SYS_SOHC_Module_Max_V 		= get_from_array(canData,2,21);

			XALT_InData.BATT_SYS_SOHC_Module_Min = ((float)get_from_array(canData,10,24))*0.1f;
			XALT_InData.BATT_SYS_SOHC_Module_Min_StrId  = get_from_array(canData,5,34)+101;
			XALT_InData.BATT_SYS_SOHC_Module_Min_VtbId 	= get_from_array(canData,5,40);
			XALT_InData.BATT_SYS_SOHC_Module_Min_V 		= get_from_array(canData,2,45);

			XALT_InData.BATT_SYS_SOHC_Module_Avg = ((float)get_from_array(canData,10,48))*0.1f;
			XALT_InData.BATT_SYS_SOHC_Module_Avg_V 		= get_from_array(canData,2,58);

			break;
		case XALT_BATT_DIAGNOSTIC_STRING:
			XALT_Timeout_Array[TIMEOUT_XALT_BATT_DIAGNOSTIC_STRING] = XALT_TIMEOUT_200MSEC;

			mux_val = get_from_array(canData,8,0)-101; // get multiplexor

			if(mux_val < MUX_MSG_COUNT){ // check bounds of array
				XALT_InData.MUX_String_ID = mux_val;
				// reinterpret 8 byte can array as uint64_t
				XALT_InData.SXX_Diag[mux_val] = *((uint64_t*)canData);
			}else{
				set_error(ERROR_XALT_CAN_MUX_OUT_OF_BOUNDS, ERROR_LOGONLY, mux_val,(*((uint64_t*)canData))>>32,(*((uint64_t*)canData)));
			}
			break;

		case XALT_BATT_SYS_SOHR:
			XALT_Timeout_Array[TIMEOUT_XALT_BATT_SYS_SOHR] = XALT_TIMEOUT_5SEC;

			XALT_InData.BATT_SYS_SOHR_Module_Max = ((float)get_from_array(canData,10,0))*0.1f;
			XALT_InData.BATT_SYS_SOHR_Module_Max_StrId  = get_from_array(canData,5,10)+101;
			XALT_InData.BATT_SYS_SOHR_Module_Max_VtbId 	= get_from_array(canData,5,16);
			XALT_InData.BATT_SYS_SOHR_Module_Max_V 		= get_from_array(canData,2,21);

			XALT_InData.BATT_SYS_SOHR_Module_Min = ((float)get_from_array(canData,10,24))*0.1f;
			XALT_InData.BATT_SYS_SOHR_Module_Min_StrId  = get_from_array(canData,5,34)+101;
			XALT_InData.BATT_SYS_SOHR_Module_Min_VtbId 	= get_from_array(canData,5,40);
			XALT_InData.BATT_SYS_SOHR_Module_Min_V 		= get_from_array(canData,2,45);

			XALT_InData.BATT_SYS_SOHR_Module_Avg = ((float)get_from_array(canData,10,48))*0.1f;
			XALT_InData.BATT_SYS_SOHR_Module_Avg_V 		= get_from_array(canData,2,58);

			break;
		case XALT_BATT_CNC_VOLT_CELL:
			XALT_Timeout_Array[TIMEOUT_XALT_BATT_CNC_VOLT_CELL] = XALT_TIMEOUT_100MSEC;

			XALT_InData.BATT_CNC_Volt_Cell_Max  = ((float)get_from_array(canData,10,0))*0.005f;
			XALT_InData.BATT_CNC_Volt_Cell_Max_StrId  = get_from_array(canData,5,10)+101;
			XALT_InData.BATT_CNC_Volt_Cell_Max_VtbId  = get_from_array(canData,5,15);
			XALT_InData.BATT_CNC_Volt_Cell_Max_CellId = get_from_array(canData,4,20);

			XALT_InData.BATT_CNC_Volt_Cell_Max  = ((float)get_from_array(canData,10,24))*0.005f;
			XALT_InData.BATT_CNC_Volt_Cell_Max_StrId  = get_from_array(canData,5,34)+101;
			XALT_InData.BATT_CNC_Volt_Cell_Max_VtbId  = get_from_array(canData,5,39);
			XALT_InData.BATT_CNC_Volt_Cell_Max_CellId = get_from_array(canData,4,44);

			XALT_InData.BATT_CNC_Volt_Cell_Avg = ((float)get_from_array(canData,10,48))*0.005f;

			XALT_InData.BATT_CNC_Volt_Cell_Max_V 	= get_from_array(canData,2,58);
			XALT_InData.BATT_CNC_Volt_Cell_Min_V 	= get_from_array(canData,2,60);
			XALT_InData.BATT_CNC_Volt_Cell_Avg_V 	= get_from_array(canData,2,62);

			break;
		case XALT_BATT_HVIL:
			XALT_Timeout_Array[TIMEOUT_XALT_BATT_HVIL] = XALT_TIMEOUT_100MSEC;

			XALT_InData.hvilState = get_from_array(canData,2,0);

			XALT_InData.BATT_HVIL_SourceVolt = (float)get_from_array(canData,8,5);
			XALT_InData.BATT_HVIL_SourceVolt_V = get_from_array(canData,2,13);

			XALT_InData.BATT_HVIL_SinkVolt = (float)get_from_array(canData,8,16);
			XALT_InData.BATT_HVIL_SinkVolt_V = get_from_array(canData,2,21);
			XALT_InData.BATT_HVIL_SinkCrrnt = get_from_array(canData,6,24);

			break;
		case XALT_BATT_VERSION:
			XALT_Timeout_Array[TIMEOUT_XALT_BATT_VERSION] = XALT_TIMEOUT_1SEC;


			mux_val = get_from_array(canData,8,0); // get multiplexor
			if(mux_val == 100){
				XALT_InData.MCU_Version_String = *((uint64_t*)canData);
			}
			else if(mux_val> 100 && mux_val <=106) { // check bounds of array
				XALT_InData.MUX_String_ID = mux_val-101;
				// reinterpret 8 byte can array as uint64_t
				XALT_InData.SXX_Version_String[mux_val] = *((uint64_t*)canData);
			}else{
				set_error(ERROR_XALT_CAN_MUX_OUT_OF_BOUNDS, ERROR_LOGONLY, mux_val,(*((uint64_t*)canData))>>32,(*((uint64_t*)canData)));
			}

			break;
		case XALT_BATT_DIAGNOSTIC_SYSTEM:
			XALT_Timeout_Array[TIMEOUT_XALT_BATT_DIAGNOSTIC_SYSTEM] = XALT_TIMEOUT_200MSEC;
			XALT_InData.BATT_Diagnostic_System_String = *((uint64_t*)canData);

			break;
		case XALT_UNKNOWN_UNUSED1:
			break;
		default:
			set_error(ERROR_XALT_UNKNOWN_MSG, ERROR_LOGONLY, canHeader.ExtId,(*((uint64_t*)canData))>>32,(*((uint64_t*)canData)));
			break;
	}

}



uint64_t get_from_array(uint8_t* array,uint8_t len,uint8_t pos){
	// MUST ONLY BE USED ON CAN ARRAY WHICH IS 8 BYTES LONG.
	return (	*((uint64_t*)array)  >> pos) & (UINT64_MAX >> (64-len));

}

