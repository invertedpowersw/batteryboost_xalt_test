/*
 * errors.c
 *
 *  Created on: May 24, 2021
 *      Author: InvertedPower
 */
#include <stdio.h>
#include <string.h>


#include "errors.h"
#include "main.h"

t_error errorLog[MAX_ERROR_COUNT];
t_errorSeverity severityLevel;
uint16_t errorCount = 0;

void reset_all_errors(){
	severityLevel = ERROR_NONE;
	errorCount = 0;
	memset(errorLog,0,sizeof(errorLog));
}

void set_error_struct(t_error e){
	if(e.sev > severityLevel){
		severityLevel = e.sev;
	}

	if(errorCount < MAX_ERROR_COUNT){
		errorLog[errorCount] = e;
		errorCount++;
	}
}

void set_error(t_errorType type,t_errorSeverity sev,uint32_t data1,uint32_t data2,uint32_t data3){
	if(sev > severityLevel){
		severityLevel = sev;
	}

	if(errorCount < MAX_ERROR_COUNT){
		for(int i = 0;i<MAX_ERROR_COUNT;i++){
			if(errorLog[i].type == type){
				return; // Quit if already logged this error
			}
		}
		errorLog[errorCount].type = type;
		errorLog[errorCount].sev = sev;
		errorLog[errorCount].data1 = data1;
		errorLog[errorCount].data2 = data2;
		errorLog[errorCount].data3 = data3;
		errorCount++;
	}
}
